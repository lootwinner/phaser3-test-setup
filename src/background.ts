export class Background extends Phaser.GameObjects.Container {

    constructor(scene: Phaser.Scene) {
        super(scene);

        this.scene.add.shader('RGB Shift Field', 0, 0, 800, 600)
        .setOrigin(0);

        this.scene.add.shader('Plasma', 0, 412, 800, 172)
        .setOrigin(0);
    }
}